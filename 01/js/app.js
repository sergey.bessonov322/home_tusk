'use strict';

const a = document.getElementById('a');
const b = document.getElementById('b');
let x;
const resultButton = document.getElementById('result');

resultButton.addEventListener('click', () => {
    const valueA = +a.value;
    const valueB = +b.value;
    console.log(valueA, valueB);
    if (valueA > valueB) {
        x = valueA + valueB / 2 * 4;
        console.log(x + " a > b");
    } else if (valueA === valueB) {
        x = 400;
        console.log(x + " a = b");
    } else if (valueA < valueB) {
        x = valueA - valueB + 2 / valueB * 4;
        console.log(x + ' a < b');
    }
});

const x02 = document.getElementById('x02');
const y02 = document.getElementById('y02');
const resultButton02 = document.getElementById('result02');

resultButton02.addEventListener('click', () => {
    const valueX02 = +x02.value;
    const valueY02 = +y02.value;
    const line1 = -3 * valueX02 + 3 * valueY02 - 9;
    const line2 = -3 * valueX02 - 3 * valueY02 + 6;
    const line3 = 6 * valueY02;
    if (line1 <= 0 && line2 <= 0 && line3 >= 0) {
        console.log("Ваша точка внутри закрашеного поля. Координаты: x:" + valueX02 + " y:" + valueY02);
    } else {
        console.log(("Ваша точка за пределами закрашеного поля. Координаты: x:" + valueX02 + " y:" + valueY02))
    }
});

const x02_2 = document.getElementById('x02_2');
const y02_2 = document.getElementById('y02_2');
const resultButton02_2 = document.getElementById('result02_2');

resultButton02_2.addEventListener('click', () => {
    const valueX02_2 = +x02_2.value;
    const valueY02_2 = +y02_2.value;
    const line1 = 2 * valueY02_2 - 2;
    const line2 = -valueX02_2 + valueY02_2;
    const line3 = valueX02_2 + valueY02_2;
    const line4 = valueX02_2 - valueY02_2;
    const line5 = 2 * valueY02_2 + 2;
    const line6 = valueX02_2 + valueY02_2;
    console.log(line1, " ", line2, " ", line3, " ", line4, " ", line5, " ", line6)
    if (valueY02_2 > 0 && line1 <= 0 && line2 >= 0 && line3 >= 0) {
        console.log("Ваша точка внутри закрашеного поля. Координаты: x:" + valueX02_2 + " y:" + valueY02_2);
    } else if (valueY02_2 < 0 && line4 >= 0 && line5 >= 0 && line6 <= 0) {
        console.log("Ваша точка внутри закрашеного поля. Координаты: x:" + valueX02_2 + " y:" + valueY02_2);
    } else {
        console.log(("Ваша точка за пределами закрашеного поля. Координаты: x:" + valueX02_2 + " y:" + valueY02_2))
    }
});

const x02_3 = document.getElementById('x02_3');
const y02_3 = document.getElementById('y02_3');
const resultButton02_3 = document.getElementById('result02_3');

resultButton02_3.addEventListener('click', () => {
    const valueX02_3 = +x02_3.value;
    const valueY02_3 = +y02_3.value;
    const cercle_X = 0;
    const cercle_Y = 0;
    const radiusCercle = 1;
    const hit = (valueX02_3 - cercle_X) ** 2 + (valueY02_3 - cercle_Y) ** 2;
    const line1 = 2 * valueX02_3 + 2 * valueY02_3 + 4;
    const line2 = -2 * valueX02_3;
    const line3 = -2 * valueY02_3;
    console.log(line1, " ", line2, " ", line3)
    if (hit <= radiusCercle ** 2) {
        console.log("Ваша точка внутри закрашеного поля. Координаты: x:" + valueX02_3 + " y:" + valueY02_3);
    } else if (line1 >= 0 && line2 >= 0 && line3 <= 0) {
        console.log("Ваша точка внутри закрашеного поля. Координаты: x:" + valueX02_3 + " y:" + valueY02_3);
    } else {
        console.log(("Ваша точка за пределами закрашеного поля. Координаты: x:" + valueX02_3 + " y:" + valueY02_3))
    }
});

const buttonThree = document.getElementById('ButtonThree');

buttonThree.addEventListener('click', () => {
    const numberOfDay = prompt('Введите номер дня недели');
    switch (numberOfDay) {
        case '1':
            alert('monday');
            break;
        case '2':
            alert('Tuesday');
            break;
        case '3':
            alert('Wednesday');
            break;
        case '4':
            alert('Thursday');
            break;
        case '5':
            alert('Friday');
            break;
        case '6':
            alert('Saturday');
            break;
        case '7':
            alert('Sunday');
            break;
        default:
            alert('Введите цыфру от 1 до 7');
            break;
    }
});

const buttonFour = document.getElementById('ButtonFour');

buttonFour.addEventListener('click', () => {
    const a = prompt("первое число");
    const b = prompt("второе число");
    if (a > b) {
        alert("a > b  " + `a: ${a}`);
    } else if (a < b) {
        alert('b > a  ' + `b: ${b}`);
    } else {
        alert('a = b');
    }
});

const buttonSix = document.getElementById('ButtonSix');

buttonSix.addEventListener('click', () => {
    const a = +prompt("Номер квартиры");
    if (a >= 1 && a <= 20) {
        alert('1 подьезд');
    } else if (a >= 21 && a <= 48) {
        alert('2 подьезд');
    } else if (a >= 49 && a <= 90) {
        alert('3 подьезд');
    } else {
        alert('введите квартиру от 1 до 90');
    }
});

const buttonSeven = document.getElementById('ButtonSeven');

buttonSeven.addEventListener('click', () => {
    const login = prompt("Login");
    const password = prompt("Password");
    if (login === "ivan" && +password === 334455) {
        alert('Welcome');
    } else if (login === "alex" && +password === 777) {
        alert('Welcome');
    } else if (login === "petr" && password === 'b5678') {
        alert('Welcome');
    } else {
        alert('error');
    }
});

const buttonEight = document.getElementById('ButtonEight');

buttonEight.addEventListener('click', () => {
    const age = +prompt("Введите возвраст");
    if (age >= 16) {
        alert("добро пожаловать");
    } else {
        alert("вход воспрещен");
    }

});

const buttonNine = document.getElementById('ButtonNine');

buttonNine.addEventListener('click', () => {
    const experience = +prompt("Введите стаж");
    if (experience >= 0 && experience < 3) {
        alert("ваша надбавка 0%");
    } else if (experience >= 3 && experience < 10) {
        alert("ваша надбавка 10%");
    } else if (experience >= 10 && experience < 20) {
        alert("ваша надбавка 20%");
    } else if (experience >= 20) {
        alert("ваша надбавка 25%");
    } else {
        alert('введите корректное значение');
    }

});

const buttonTen = document.getElementById('ButtonTen');

function declinationOfProduct(product){
    const name = 'товар';
    switch (product) {
        case 0:
            alert(name + 'ов');
            break;
        case 1:
            alert(name);
            break;
        case product >= 2 && product <= 4 ? product:true:
            alert(name + 'a');
            break;
        case product >= 6 ? product: true:
            alert(name + "ов");
            break;
        default:
            alert("введите корректное значение");
            break;

    }
}

buttonTen.addEventListener('click', () => {
    const product = +prompt("Введите кол-во продуктов");
    declinationOfProduct(product);
});